<?php
	namespace App\Entity;
	use Core\Entity\Entity;

	class PostEntity extends Entity{

		public function getUrl(){
			return '/posts-show/' . $this->id;
		}

		public function getExtrait(){
			$html = '<p>' . substr($this->description, 0, 200) . '...</p>';
			$html .= '<p><a href="'. $this->getURL() .'">Voir la suite</a></p>';
			return $html;
		}
	}