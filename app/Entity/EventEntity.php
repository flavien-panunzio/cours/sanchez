<?php
	namespace App\Entity;
	use Core\Entity\Entity;

	class EventEntity extends Entity{
		public function getExtrait0(){
			$html = substr($this->description, 0, 100) . ' ...';
			return $html;
		}
		public function getExtrait1(){
			$html = substr($this->description, 0, 200) . ' ...';
			return $html;
		}
	}