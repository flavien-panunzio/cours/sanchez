<?php 
	namespace App\Table;
	use Core\Table\Table;
	class PostTable extends Table {

		protected $table = 'portfolio';
		
		public function last(){
			return $this->query("
				SELECT portfolio.id, portfolio.titre, portfolio.description, categories.titre as categorie
				FROM portfolio 
				LEFT JOIN categories ON category_id = categories.id
				ORDER BY portfolio.id DESC");
		}

		public function findWithCategory($id){
			return $this->query("
				SELECT portfolio.id, portfolio.titre, portfolio.description, categories.titre as categorie
				FROM portfolio 
				LEFT JOIN categories ON category_id = categories.id
				WHERE portfolio.id = ?", [$id], true);
		}

		public function lastByCategory($category_id){
			return $this->query("
				SELECT portfolio.id, portfolio.titre, portfolio.description, categories.titre as categorie
				FROM portfolio 
				LEFT JOIN categories ON category_id = categories.id
				WHERE portfolio.category_id = ?
				ORDER BY portfolio.id DESC", [$category_id]);
		}
	}