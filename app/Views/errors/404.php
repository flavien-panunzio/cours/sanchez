<?php header('HTTP/1.0 404 Not Found');  
?>
<div class="erreur404">
	<img src="/images/general/404.gif" alt="Time Tracking System 404 Animation">
	<h2>Erreur 404</h2>
	<p>Page non trouvée</p>
	<a href="/">Revenir à l'accueil</a>
</div>