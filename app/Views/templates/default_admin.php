<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<meta name="author" content="Panunzio Flavien" />
		<meta name="copyright" content="© 2018 Flavien Panunzio" />

		<meta name="title" content="<?=$this->titre?>" />
		<title>Administration | <?=$this->titre?></title>

		<link rel="icon" href="/images/favicon.png" type="image/x-icon"/>

		<!-- CSS -->
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="/style/default-admin/css/style.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	</head>
	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
				<a class="navbar-brand" href="/admin-posts-index">Administration</a>
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="/admin-posts-index">Photos</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/admin-event-index">Évenements</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/admin-media-index">Médias</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/users-logout">Déconnexion</a>
					</li>
				</ul>
			</nav>
		</header>
		<main role="main" class="container">
			<div class="starter-template">
				<?= $content; ?>
			</div>
		</main>
		<footer>
			
		</footer>
	</body>
	<!-- JAVASCRIPT -->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
</html>