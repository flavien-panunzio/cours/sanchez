<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<?=$this->indexation?>

		<meta name="author" content="Panunzio Flavien" />
		<meta name="copyright" content="© 2018 Flavien Panunzio" />

		<!-- COULEUR BAR URL IPHONE ANDROID WINDOWS-PHONE -->
		<meta name="apple-mobile-web-app-status-bar-style" content="#F8B62E" />
		<meta name="theme-color" content="#F8B62E" />
		<meta name="msapplication-navbutton-color" content="#F8B62E">

		<!-- META OpenGraph -->
		<meta property="og:title" content="<?=$this->titre?>" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?=$_SERVER['REQUEST_URI']?>" />
		<meta property="og:image" content="<?=$_SERVER['HTTP_HOST']?>/images/presentation.jpg" />
		<meta property="og:description" content="<?=$this->description?>" />

		<!-- META TWITTER -->
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:site" content="<?=$_SERVER['REQUEST_URI']?>" />
		<meta name="twitter:title" content="<?=$this->titre?>" />
		<meta name="twitter:description" content="<?=$this->description?>" />
		<meta name="twitter:image" content="<?= $_SERVER['HTTP_HOST']; ?>/images/presentation.jpg" />
		
		<title><?=$this->titre?></title>
		<meta name="description" content="<?=$this->description?>" />
		<meta name="keywords" content="artiste, photographe, voiture">

		<link rel="icon" href="/images/favicon.png" type="image/x-icon"/>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<meta name="google-site-verification" content="" />

		<!-- CSS -->
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="/style/default/css/style.css">
	</head>
	<body>
		<?= $content; ?>
		
		<footer role="contentinfo">
			<i class="fas fa-info-circle fa-2x"></i>
			<div class="bandeau">
				<p><a href="#">Mentions Légales</a></p>
				<p>Site créé par <a href="http://flavien-panunzio.tk/" target="_blank">Flavien Panunzio</a> | &copy 2018 TEMPLATE</p>
				<p><a href="/posts-contact">Contact</a></p>
			</div>
		</footer>
	</body>

	<!-- JAVASCRIPT -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>

	<script src="//cdn.jsdelivr.net/jquery.color-animation/1/mainfile"></script>
	<script type="text/javascript" src="/js/script.js"></script>
</html>