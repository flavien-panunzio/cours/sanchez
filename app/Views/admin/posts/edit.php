<div class="photos">
	<form method="post">
		<div class="form-group">
			<label for="titre">Titre de la photo</label>
			<input type="text" class="form-control" id="titre" name="titre" value="<?=$post->titre?>">
		</div>
		<div class="form-group">
			<label for="description">Description</label>
			<textarea class="form-control" id="description" name="description" rows="3"><?=$post->description?></textarea>
		</div>
		<div class="form-group">
			<label for="categ">Catégorie</label>
			<select class="form-control" name="category_id" id="categ">
				<option value="<?=$post->category_id ?>"><?=$categories[$post->category_id]?></option>
				<?php foreach ($categories as $key => $value) :
					if ($key!=$post->category_id) : ?>
						<option value="<?=$key ?>"><?=$value?></option>
					<?php endif;
				endforeach; ?>
			</select>
		</div>
		<div class="image">
			<img src="<?= $post->photo?>">
		</div>
		<input id="input-img" type="hidden" name="photo" value="<?= $post->photo;?>">
		<button type="submit" class="btn btn-primary">Sauvegarder</button>
	</form>
	<div class="modal fade" id="chgphoto" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="divimgUploads">
						<?php foreach ($files1 as $value1) :?>
								<div class="imgUploads1" >
									<img src="/images/collection/<?=$value1;?>">
								</div>
						<?php endforeach; ?>
						<?php foreach ($files2 as $value2) :?>
								<div class="imgUploads2">
									<img src="/images/sport/<?=$value2;?>">
								</div>
						<?php endforeach; ?>
						<?php foreach ($files3 as $value3) :?>
								<div class="imgUploads3">
									<img src="/images/autres/<?=$value3;?>">
								</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(".image img").on('click', function() {
		$('#chgphoto').modal('show');
		button = $(event.relatedTarget);

		$(".imgUploads1,.imgUploads2,.imgUploads3").click(function(event) {
			image=$(this).children().attr('src');
			$("#input-img").attr('value', image);
			$('.image img').attr('src', image);
			$('#chgphoto').modal('hide');
		});
	});

</script>