<h1>Administrer les articles</h1>
<p><a href="/admin-posts-add" class="btn btn-success">Ajouter</a></p>
<table class="table">
	<thead>
		<tr>
			<td>Titre</td>
			<td>Actions</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($posts as $post) : ?>
			<tr>
				<td><?= $post->titre;?></td>
				<td>
					<a class="btn btn-primary" href="/admin-posts-edit/<?= $post->id; ?>">Éditer</a>
					<form method="post" action="/admin-posts-delete" style="display: inline-block;">
						<input type="hidden" name="id" value="<?= $post->id ?>">
						<button type="submit" class="btn btn-danger">Supprimer</button>
					</form>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>