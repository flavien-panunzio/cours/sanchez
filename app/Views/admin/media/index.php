<div class="app-title">
	<form action="/admin-media-upload<?=$_GET['id']?>" method="post" enctype="multipart/form-data">
		<div class="custom-file">
			<input type="file" class="custom-file-input" id="customFile" name="fileToUpload" required>
			<label class="custom-file-label" for="customFile">Ajouter une image</label>
		</div>
		<button class="btn btn-primary" type="submit">Envoyer</button>
	</form>
</div>
<div class="medias">
	<?php if($_GET["id"]!="/") : ?>
		<a class="btn btn-primary" href="/admin-media-index" role="button">Retour</a>
	<?php endif; 
	if($files!=null) : ?>
		<div class="tuiles">
			<?php foreach ($files as $value) :
				if(strstr($value,'.')): ?>
					<div class="div-img" id="<?=$value;?>">
						<?php if($folder==='/') $folder = '';?>
						<img src="/images<?=$folder.'/'.$value;?>">
					</div>
				<?php else: 
					if ($value=='trash') :?>
						<div class="div-trash">
							<a class="folder" href="/admin-media-index/<?=$value;?>">
								<i class="fas fa-trash fa-10x"></i>
								<?=$value;?>
							</a>
						</div>
					<?php else :?>
						<div class="div-folder">
							<a class="folder" href="/admin-media-index/<?=$value;?>">
								<i class="fas fa-folder fa-10x"></i>
								<?=$value;?>
							</a>
						</div>
					<?php endif; 
				endif;?>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<img>
				</div>
				<form method="get">
					<div class="modal-body">
						<div class="form-group">
							<label for="new_name">Renomer l'image</label>
							<input type="text" class="form-control" id="newname" name="newname">
							<small id="emailHelp" class="form-text text-muted">ATTENTION ! Toutes modifications de noms peuvent avoir des conséquences sur le bon foctionnement de votre site web</small>
							<input type="hidden" name="oldname" id="oldname">
						</div>
					</div>
					<div class="modal-footer">
						<a type="button" class="btn btn-secondary"><i class="fas fa-trash"></i></a>
						<div>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
							<input type="submit" class="btn btn-primary" value="Sauvegarder">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(".div-img img").on('click', function() {
		$('#Modal').modal('show');
		img=$(this).attr('src');
		$('.modal-header img').attr('src', img);
		$('.modal-body #newname').attr('value', img);
		$('.modal-body #oldname').attr('value', img);
		$('.modal-content form').attr('action', '/admin-media-rename'+img);
		$('.modal-footer a').attr('href', '/admin-media-delete'+img);
	});
	$('#customFile').on('change',function(){
		var fileName = $(this).val();
		$(this).next('.custom-file-label').html(fileName);
	})
</script>