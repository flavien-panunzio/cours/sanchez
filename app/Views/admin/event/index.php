<h1>Administrer les articles</h1>
<p><a href="/admin-event-add" class="btn btn-success">Ajouter</a></p>
<table class="table">
	<thead>
		<tr>
			<td>Titre</td>
			<td>Actions</td>
			<td>Mise en avant</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($event as $post) : ?>
			<tr>
				<td><?= $post->nom;?></td>
				<td>
					<a class="btn btn-primary" href="/admin-event-edit/<?= $post->id; ?>">Éditer</a>
					<form method="post" action="/admin-event-delete" style="display: inline-block;">
						<input type="hidden" name="id" value="<?= $post->id ?>">
						<button type="submit" class="btn btn-danger">Supprimer</button>
					</form>
				</td>
				<td class="text-center"><?= $post->avant;?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>