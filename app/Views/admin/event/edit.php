<form method="post">
	<div class="form-group">
		<label for="nom">Nom de l'évènement</label>
		<input type="text" class="form-control" id="nom" name="nom" value="<?=$event->nom?>">
	</div>
	<div class="form-group">
		<label for="description">Description</label>
		<textarea class="form-control" id="description" name="description" rows="3"><?=$event->description?></textarea>
	</div>
	<div class="form-group">
		<label for="photo">Lien de la photo</label>
		<input type="text" class="form-control" id="photo" name="photo" value="<?=$event->photo?>">
	</div>
	<div class="row">
		<div class="col">
			<label for="date_debut">Date de début</label>
			<input type="date" class="form-control" id="date_debut" name="date_debut" value="<?=$event->date_debut?>">
		</div>
		<div class="col">
			<label for="date_fin">Date de fin</label>
			<input type="date" class="form-control" id="date_fin" name="date_fin" value="<?=$event->date_fin?>">
		</div>
	</div>
	<div class="form-group">
		<label for="avant">Mise en avant</label>
		<select class="form-control" name="avant" id="avant">
			<?php if ($event->avant==0) : ?>
				<option value="0">NON</option>
				<option value="1">OUI</option>
			<?php else: ?>
				<option value="1">OUI</option>
				<option value="0">NON</option>
			<?php endif; ?>
		</select>
	</div>
	<button type="submit" class="btn btn-primary">Sauvegarder</button>
</form>