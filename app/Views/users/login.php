<?php if($errors): ?>
	<div class="alert alert-danger">
		Identifiants incorrects
	</div>
<?php endif;?>
<form method="post">
	<div class="form-group">
		<label for="username">Pseudo</label>
		<input type="text" class="form-control" name="username" id="username" required>
		<label for="password">MotDePasse</label>
		<input type="password" class="form-control" name="password" id="password" required>
	</div>
	<button class="btn btn-primary">Envoyer</button>
</form>