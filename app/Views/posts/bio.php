<a class="return" href="/"><i class="fas fa-chevron-circle-left fa-2x"></i></a>
<main role="main" class="bio">
	<hr class="no-padding">
	<h1>BIO</h1>
	<div class="content">
		<div class="texte">
			<h2>MON PARCOURS</h2>
			<p>
				Photographe qui exerce depuis 2001, Roberto habite à Saint Etienne et réalise alors des photos de portraits et de mariages pour divers clients. Cependant, outre ces photos standards, il prend également en photo sa passion, l’automobile, mais ne vend pas ces photos, bien qu’elles soient beaucoup plus artistiques que celles qu’il a l’habitude de produire.
			</p>
			<p>
				En 2010, il est repéré par une entreprise automobile intéressée par les photos de son compte Pinterest et est payé pour prendre une dizaine de photos de la nouvelle voiture de la marque. Depuis ce jour, il est sollicité par d’autres marques qui lui achètent ses photos d’automobiles. Roberto devient peu à peu un photographe renommé dans le monde de l’automobile et devient une référence pour les entreprises automobiles et même pour tout autre particulier qui aurait besoin de photos dans ce thème. Il veut toucher une cible large qui va des grandes marques automobiles aux associations organisant des évènements automobiles.
			</p>
			<p>
				Roberto est présent sur de nombreux évènements automobiles dans lesquels il peut s’exerver et prendre en photo sa passion en toute liberté. Fan de l’Espagne il suit de nombreuses courses à Barcelone et vous pourrez souvent le rencontrer lors de ces évènements pour de plus amples informations.
			</p>
		</div>
		<div class="image">
			<img src="/images/bio.jpg">
		</div>
		<div class="fiche">
			<h3> Roberto SANCHEZ</h3>
			<p class="no-padding">32 ans</p>
			<p class="no-padding">auto-entrepreneur</p>
			<p class="no-padding">878 photos</p>
			<p class="no-padding">27 vidéos</p>
		</div>
	</div>
</main>