<a class="return" href="/"><i class="fas fa-chevron-circle-left fa-2x"></i></a>
<main role="main" class="event">
	<hr class="no-padding sticky-top">
	<div class="up">
		<h1 class="no-padding">ÉVÉNEMENTS</h1>
	</div>

	<div class="grille">
		<?php foreach ($event as	$value) : ?>
			<div class="tuile avant<?=$value->avant?>">
				<img src="<?=$value->photo?>">
				<div>
					<h2><?=$value->nom?></h2>
					<p class="description"><?=$value->{'extrait'.$value->avant}?></p>
					<a href="" data-toggle="modal" data-target="#modalEvent" data-nom="<?=$value->nom?>" data-description="<?=$value->description?>" data-photo="<?=$value->photo?>" data-datedebut="<?=$value->date_debut?>" data-datefin="<?=$value->date_fin?>">En savoir plus</a>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="modal fade" id="modalEvent" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<img>
				<h2></h2>
				<table>
					<td class="datedebut"></td>
					<td class="datefin"></td>
				</table>
				<p class="descr"></p>
			</div>
		</div>
	</div>
</main>