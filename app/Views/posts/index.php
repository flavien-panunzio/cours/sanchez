<main role="main" class="index container-fluid">
	<div class="row">
		<div class="image col-6">
			<div class="titre">
				<h1>Roberto<br>&emsp;SANCHEZ</h1>
			</div>
		</div>
		<div class="menu col-6">
			<div class="titre">
				<h1>Roberto<br>&emsp;SANCHEZ</h1>
				<hr>
				<label>Photographe Automobile</label>
			</div>
			<ul class="menu-list">
				<li><a href="/posts-bio">BIO</a></li>
				<li><a href="/posts-event">ÉVÉNEMENTS</a></li>
				<li><a href="/posts-photos">PHOTOS</a></li>
				<li><a href="/posts-contact">CONTACT</a></li>
			</ul>
		</div>
	</div>
</main>