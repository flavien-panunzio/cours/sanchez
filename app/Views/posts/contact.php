<a class="return" href="/"><i class="fas fa-chevron-circle-left fa-2x"></i></a>
<main role="main" class="contact no-padding">
	<div class="row no-padding">
		<div class="menu col-6">
			<div class="titre">
				<h1>Roberto<br>&emsp;SANCHEZ</h1>
				<a href="tel:0651837122">06.51.83.71.22</a>
				<a href="mailto:robertosan@gmail.com">robertosan@gmail.com</a>
				<div>
					<a href="http://www.facebook.com"><i class="fab fa-facebook-square"></i></a>
					<a href="https://www.instagram.com/logoplace/"><i class="fab fa-instagram"></i></a>
					<a href="https://unsplash.com/@georgesultan"><i class="fas fa-camera"></i></a>
				</div>
			</div>
		</div>
		<div class="image col-6">
			<div class="titre">
				<h1>Roberto<br>&emsp;SANCHEZ</h1>
				<hr>
				<label>Photographe Automobile</label>
			</div>
		</div>
	</div>
</main>