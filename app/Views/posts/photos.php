<a class="return" href="/"><i class="fas fa-chevron-circle-left fa-2x"></i></a>
<main role="main" class="photos">
	<hr class="no-padding sticky-top">
	<div class="up">
		<h1 class="no-padding">PHOTOS</h1>
	</div>
	<div class="filtre">
		<div class="collections">
			<img src="/images/collection.jpg">
			<p class="text-uppercase text-center">Collection</p>
		</div>
		<div class="sportives">
			<img src="/images/sportives.jpg">
			<p class="text-uppercase text-center">Sportives</p>
		</div>
		<div class="divers">
			<img src="/images/divers.jpg">
			<p class="text-uppercase text-center">Divers</p>
		</div>	
	</div>
	<div class="container">
		<h2 class="categ no-padding" id="ancre"></h2>
		<div class="grille">
			<?php foreach ($photos as  $value) : ?>
				<div class="tuile categ_<?=$value->category_id?>">
					<img src="<?=$value->photo?>" alt="<?= str_replace($search,'',$value->photo) ?>" data-title="<?=$value->titre?>"  data-desc="<?=$value->description?>">
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</main>