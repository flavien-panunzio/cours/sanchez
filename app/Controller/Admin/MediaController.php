<?php 
	namespace App\Controller\Admin;

	class MediaController extends AppController{

		public function __construct(){
			parent::__construct();
		}

		public function index(){
			if (!isset($_GET['id'])) {
				$_GET['id']="/";
			}
			else
				$_GET['id']='/'.$_GET['id'];
			$files=$this->readDIR('/'.$_GET['id']);
			$folder=$_GET['id'];
			$this->render('admin.media.index', compact('files', 'folder'));
		}
		public function upload(){
			if (!isset($_GET['id'])) {
				$_GET['id']="/";
			}
			else
				$_GET['id']='/'.$_GET['id'].'/';
			$target_dir = ROOT."/public/images".$_GET['id'];
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			// Check if image file is a actual image or fake image
			if(isset($_POST["submit"])) {
				$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
				if($check !== false) {
					echo "File is an image - " . $check["mime"] . ".";
					$uploadOk = 1;
				} else {
					echo "File is not an image.";
					$uploadOk = 0;
				}
			}
			$target_file = str_replace(' ', '_', $target_file);
			// Check if file already exists
			if (file_exists($target_file)) {
				echo "Sorry, file already exists.";
				$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 2097152) {
				echo "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
			&& $imageFileType != "gif" ) {
				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
				$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				echo "Sorry, your file was not uploaded.";
			// if everything is ok, try to upload file
			} else {
				if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
					header("Location: /admin-media-index".$_GET['id']);
					die;
				} else {
					echo "Sorry, there was an error uploading your file.";
				}
			}
		}

		public function delete(){
			$file=str_replace("/admin-media-delete/", '', $_SERVER["REQUEST_URI"]);
			$file_name=explode('/', $file);
			$file_name=end($file_name);
			rename($file, 'images/trash/'.$file_name);
			header("Location: /admin-media-index");
			die;
		}

		public function rename(){
			$old=substr($_GET['oldname'], 1);
			$new=str_replace(' ', '_', substr($_GET['newname'], 1));
			rename($old , $new);
			header("Location: /admin-media-index");
			die;
		}
	}