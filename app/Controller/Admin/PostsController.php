<?php 
	namespace App\Controller\Admin;

	class PostsController extends AppController{

		public function __construct(){
			parent::__construct();
			$this->loadModel('Post');
			$this->loadModel('Category');
		}

		public function index(){
			$posts = $this->Post->last();
			$this->render('admin.posts.index', compact('posts'));
		}

		public function add(){
			if (!empty($_POST)) {
				$result = $this->Post->create([
					'titre' => $_POST['titre'],
					'description' => $_POST['description'],
					'category_id' => $_POST['category_id'],
					'photo' => $_POST['photo']
				]);
				if ($result) {
					return $this->index();
				}
			}
			$post=(object)array();
			$post->titre='';
			$post->description='';
			$post->category_id='3';
			$post->photo='/images/default.jpg';
			$categories = $this->Category->extract('id', 'titre');

			$files1=$this->readDIR('collection');
			$files2=$this->readDIR('sport');
			$files3=$this->readDIR('autres');
			$this->render('admin.posts.edit', compact('categories','post','files1','files2','files3'));
		}

		public function edit(){
			if (!empty($_POST)) {
				$result = $this->Post->update($_GET['id'], [
					'titre' => $_POST['titre'],
					'description' => $_POST['description'],
					'category_id' => $_POST['category_id'],
					'photo' => $_POST['photo']
				]);
				if ($result) {
					return $this->index();
				}
			}
			$post = $this->Post->find($_GET['id']);
			$categories = $this->Category->extract('id', 'titre');

			$files1=$this->readDIR('collection');
			$files2=$this->readDIR('sport');
			$files3=$this->readDIR('autres');

			$this->render('admin.posts.edit', compact('categories','post','files1','files2','files3'));
		}

		public function delete(){
			if (!empty($_POST)) {
				$result = $this->Post->delete($_POST['id']);
				return $this->index();
			}
		}

	}