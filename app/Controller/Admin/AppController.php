<?php 
	namespace App\Controller\Admin;
	use \App;
	use Core\Auth\DBAuth;


	class AppController extends \App\Controller\AppController{

		protected $template = 'default_admin';

		public function __construct(){
			parent::__construct();
			$app = App::getInstance();
			$auth = new DBAuth($app->getDb());
			if (!$auth->logged()) {
				$this->forbidden();
			}
			$this->indexation='<meta name="robots" content="noindex">';
		}
		
		public function readDIR($DIRname=''){
			$i=0;
			if($dossier = opendir('images/'.$DIRname)){
				while(false !== ($fichier = readdir($dossier))) {
					if($fichier != '.' && $fichier != '..'){
						$fichiers[$i]=$fichier;
						$i++;
					}
				}
				if (isset($fichiers)) {
					return $fichiers;
				}
			}
		}
	}