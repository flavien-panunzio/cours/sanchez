<?php 
	namespace App\Controller\Admin;

	class EventController extends AppController{

		public function __construct(){
			parent::__construct();
			$this->loadModel('Event');
		}

		public function index(){
			$event = $this->Event->all();
			$this->render('admin.event.index', compact('event'));
		}

		public function add(){
			if (!empty($_POST)) {
				$result = $this->Event->create([
					'nom' => $_POST['nom'],
					'photo' => $_POST['photo'],
					'description' => $_POST['description'],
					'date_debut' => $_POST['date_debut'],
					'date_fin' => $_POST['date_fin'],
					'avant' => $_POST['avant']
				]);
				return $this->index();
			}
			$event = new \stdClass();
			$event->nom = "";
			$event->description = "";
			$event->avant = "0";
			$event->photo = "";
			$event->date_debut = "";
			$event->date_fin = "";
			$this->render('admin.event.edit', compact('event'));
		}

		public function edit(){
			if (!empty($_POST)) {
				$result = $this->Event->update($_GET['id'], [
					'nom' => $_POST['nom'],
					'photo' => $_POST['photo'],
					'description' => $_POST['description'],
					'date_debut' => $_POST['date_debut'],
					'date_fin' => $_POST['date_fin'],
					'avant' => $_POST['avant']
				]);
				return $this->index();
			}
			$event = $this->Event->find($_GET['id']);
			$this->render('admin.event.edit', compact('event'));
		}

		public function delete(){
			if (!empty($_POST)) {
				$result = $this->Event->delete($_POST['id']);
				return $this->index();
			}
		}

	}