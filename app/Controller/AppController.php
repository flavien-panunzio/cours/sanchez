<?php 
	namespace App\Controller;
	use Core\Controller\Controller;
	use \App;

	class AppController extends Controller{

		protected $template = 'default';

		public function __construct(){

			if(strrpos($_SERVER['REQUEST_URI'], 'admin')!=false) {
	    		$this->template = 'default_admin';
	    	}elseif(strrpos($_SERVER['REQUEST_URI'], 'users')!=false) {
	    		$this->template = 'default_users';
	    	}else{
	    		$this->template = 'default';
	    	}
			$this->viewPath = ROOT . '/app/Views/';
		}

		protected function loadModel($model_name){
			$this->$model_name = App::getInstance()->getTable($model_name);
		}
		
	}