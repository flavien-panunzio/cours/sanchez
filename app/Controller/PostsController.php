<?php 
	namespace App\Controller;
	use Core\Controller\Controller;
	use Core\Table\Table;

	class PostsController extends AppController{

		public function __construct(){
			parent::__construct();
			$this->loadModel('Post');
			$this->loadModel('Category');
			$this->loadModel('Event');
		}

		public function index(){
			$this->render('posts.index');
		}

		public function photos(){
			$photos = $this->Post->all();
			$search=array('.jpg','.png','.gif','/images/collection/','/images/autres/','/images/sport/');

			$this->titre='Photos | '.$this->titre;
			$this->render('posts.photos', compact('photos','search'));
		}

		public function event(){
			$event = $this->Event->all();

			$this->titre='Photos | '.$this->titre;
			$this->render('posts.event', compact('event','search'));
		}

		public function bio(){
			$this->titre='Bio | '.$this->titre;
			$this->render('posts.bio');
		}

		public function contact(){
			$this->titre='Contact | '.$this->titre;
			$this->render('posts.contact');
		}
	}