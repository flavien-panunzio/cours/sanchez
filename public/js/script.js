$(document).ready(function(){
	//target _blank	----------------------------------------------------------------
	$("a[href^='http://']").attr("target","_blank");
	$("a[href^='https://']").attr("target","_blank");
	
	//FILTRES		----------------------------------------------------------------
	$(".filtre div").click(function(e) {
		e.preventDefault();
		var aid = "#ancre";
		$('html,body').animate({scrollTop: $(aid).offset().top},'slow');
	});
	$('.collections').click(function(){
		$(".categ_1").removeClass('d-none');
		$(".categ_2").addClass('d-none');
		$(".categ_3").addClass('d-none');
		$(".categ").text('Collections');
	});
	$('.sportives').click(function(){
		$(".categ_1").addClass('d-none');
		$(".categ_2").removeClass('d-none');
		$(".categ_3").addClass('d-none');
		$(".categ").text('Sportives');
	});
	$('.divers').click(function(){
		$(".categ_1").addClass('d-none');
		$(".categ_2").addClass('d-none');
		$(".categ_3").removeClass('d-none');
		$(".categ").text('Divers');
	});

	//ZOOM PHOTOS	----------------------------------------------------------------
	$(".photos .tuile img").click(function(){
		var url = $(this).attr('src');
		var titre = $(this).data('title');
		var desc = $(this).data('desc');
		$("body").append('<div class="fondapercu"><div class="content"><img src='+url+'><div class="description"><p class="titre">'+titre+'</p><p class="desc">'+desc+'</p></div></div></div>');
		$(".fondapercu").hide(0,ouvrirapercu);
		fermerapercu();
	});
	function ouvrirapercu () {
		$(".fondapercu").fadeIn("slow");
	}
	function fermerapercu () {
		$(".fondapercu").click(function(){
			$(".fondapercu").fadeOut("slow",supprimer);
		});
		function supprimer () {
			$(".fondapercu").remove();
		}
	}

	//MENTIONS		----------------------------------------------------------------
	$('footer i').click(function(){
		var taille = $('footer').css('right');
		if (taille!='-25px') {
			$("footer").animate({'right': '-25px', backgroundColor: "#242424"}, 1000);
			$("footer").animate({'color': "white"});
		}
		else{
			$("footer").animate({'right': '-97vw', backgroundColor: 'rgba(0,0,0,0)'}, 1000);
			$("footer").animate({'color': "#242424"});
		}
	});

	//HOVER INDEX	----------------------------------------------------------------
	$('.index li').hover(function() {
		$(this).animate({'color': '#F8B62E', 'font-size': '3.5em'}, 200);
		$(this).children('a').animate({'color': '#F8B62E'}, 200);
	}, function() {
		$(this).animate({'color': '#000', 'font-size': '3em'}, 200);
		$(this).children('a').animate({'color': '#000'}, 200);
	});

	//modal
	$('#modalEvent').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)
		var modal = $(this)

		var nom = button.data('nom') 
		var photo = button.data('photo')
		var description = button.data('description')
		var datedebut = button.data('datedebut')
		var datefin = button.data('datefin') 
		
		modal.find('h2').text(nom)
		modal.find('img').attr('src', photo)
		modal.find('.descr').text(description)
		modal.find('.datedebut').text(datedebut)
		modal.find('.datefin').text(datefin)
		modal.find('img').attr('src', photo)
	})
});