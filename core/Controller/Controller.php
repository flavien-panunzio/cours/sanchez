<?php

	namespace Core\Controller;

	class Controller{

		protected $viewPath;
		protected $template;
		protected $titre='Roberto SANCHEZ';
		protected $description='Ceci est le site de Roberto Sanchez, il contient toutes ses créations et ses événements à venir.';
		protected $indexation='';

		protected function render($view, $variables = []){
			$titre=explode('-',$_SERVER['REQUEST_URI']);
			ob_start();
			extract($variables);
			require ($this->viewPath . str_replace('.','/',$view) . '.php');
			$content = ob_get_clean();
			require ($this->viewPath. 'templates/' . $this->template . '.php');
		}

		protected static function forbidden(){
			header("HTTP/1.0 403 Forbidden");
			header("Location: /users-login");
			die;
		}

		protected function loadModel($model_name){
			$this->$model_name = App::getInstance()->getTable($model_name);
		}
	}