-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 09 oct. 2018 à 05:27
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sanchez`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `titre`) VALUES
(1, 'collection'),
(2, 'sportive'),
(3, 'autre');

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `photo` varchar(255) NOT NULL,
  `avant` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio`
--

DROP TABLE IF EXISTS `portfolio`;
CREATE TABLE IF NOT EXISTS `portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `photo` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `portfolio`
--

INSERT INTO `portfolio` (`id`, `titre`, `description`, `photo`, `category_id`) VALUES
(1, 'Citroên DS rouge', 'Photo voiture rouge citroen ds.', '/images/collection/george-sultan-1054461-unsplash.jpg', 1),
(2, 'jolie Voiture jaune', 'Petite voiture jaune de marque inconnue.', '/images/collection/george-sultan-1054464-unsplash.jpg', 1),
(3, 'Grosse voiture jaune', 'Voiture jaune vue de l\'intérieur.', '/images/collection/george-sultan-1054456-unsplash.jpg', 1),
(4, 'Voiture rouge 2', 'voiure rouge old school', '/images/collection/george-sultan-1053301-unsplash.jpg', 1),
(5, 'voiture Renault', 'Voiture sport ancienne génération Renault', '/images/collection/george-sultan-1053282-unsplash.jpg', 1),
(6, 'stade Barcelone', 'Stade de Barcelone.', '/images/autres/george-sultan-1067442-unsplash.jpg', 3),
(7, 'stade voitures', 'parking de voitures', '/images/autres/george-sultan-1067455-unsplash.jpg', 3),
(8, 'route', 'Route aride', '/images/autres/marcelo-quinan-37437-unsplash.jpg', 3),
(9, 'route montagne', 'Route dans la montagne en allant vers la neige', '/images/autres/paul-gilmore-258257-unsplash.jpg', 3),
(10, 'Stade course', 'Photo d\'un stade de course de voiture.', '/images/autres/reynier-carl-685971-unsplash.jpg', 3),
(11, 'Voiture en mouvement', 'Voiture de sport rouge en mouvement.', '/images/sport/george-sultan-1068416-unsplash.jpg', 2),
(12, 'Ferrari rouge', 'Belle Ferrari de course rouge.', '/images/sport/george-sultan-1074164-unsplash.jpg', 2),
(13, 'voiture de course', 'Voiture de course noire prête à la course.', '/images/sport/george-sultan-1076192-unsplash.jpg', 2),
(14, 'Voiture en mouvement noir et blanc', 'Voiture ne mouvement dans une course prise en noir et blanc.', '/images/sport/george-sultan-1076193-unsplash.jpg', 2),
(15, 'voiture numero 9', 'Voiture rouge numéro 9 prête à partir en course.', '/images/sport/george-sultan-1084289-unsplash.jpg', 2);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'demo', '89e495e7941cf9e40e6980d14a16bf023ccd4c91');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
